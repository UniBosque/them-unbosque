
var map,
	cities = ['Bogotá', 'Chía'],
	currentMap = cities[0],
	coordsBogota = new google.maps.LatLng(4.7102000, -74.0308118),
	coordsChia = new google.maps.LatLng(4.8453106, -74.0319003),
	storeLastOverlayBogota,
	storeLastOverlayChia,
	currentPosition,
	boundsBogota = {
		north: 4.711803888519607,
		south: 4.708596107781068,
		east: -74.02845145606688,
		west: -74.03317214393309
	},
	boundsChia = {
		north: 4.846346240929798,
		south: 4.844274957483376,
		east: -74.03072012803347,
		west: -74.03308047196657
	};

function geoLocationCustom() {
	// Try HTML5 geolocation.
    if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            currentPosition = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            //infoWindow.setPosition(pos);
            //infoWindow.setContent('Location found.');
            //map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());	  
    }
}

geoLocationCustom();

function CenterControl(controlDiv, map) {
	// Set CSS for the control border.
	var controlUI = document.createElement('div');
	controlUI.style.backgroundImage = "url(/themes/custom/seed_ubosque/map-svg/you-are-here-button.svg)";
	controlUI.style.width = '30px';
	controlUI.style.height = '30px';
	// controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	controlUI.style.cursor = 'pointer';
	controlUI.style.marginRight = '10px';
	// controlUI.style.textAlign = 'center';
	controlUI.title = 'Ver su ubicación actual';
	controlDiv.appendChild(controlUI);


	controlUI.addEventListener('click', function() {
		var 
		markerLocation = new google.maps.Marker({
			position: new google.maps.LatLng(currentPosition.lat, currentPosition.lng),
			icon: new google.maps.MarkerImage('/themes/custom/seed_ubosque/map-svg/you-are-here.svg', null, null, null, new google.maps.Size(42.84,53)),
			animation: google.maps.Animation.DROP
		});
				
		markerLocation.setMap(map);
				
		var 
		locationInfoWindow = new google.maps.InfoWindow({
			content: "<div style='font-family: acherus_grotesqueblack; font-size: 16px; line-height: 26px; letter-spacing: -0.5px; padding: 5px 5px 0 15px; overflow: hidden'>Usted está aquí</div>",
			html: true
		});
				
		locationInfoWindow.open(map, markerLocation);
		map.setCenter(currentPosition);
	});	
}


function initMap() {
	var mapOptions = {
		center: coordsBogota,
		zoom: 18,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		zoomControl: true,
        zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_TOP
        },
		
	};
	
	function mapBogota() {
		map.panTo(coordsBogota);
		map.setZoom(18);
	};
	
	function mapChia() {
		map.panTo(coordsChia);
		map.setZoom(19);
	};

	map = new google.maps.Map(document.getElementById('map'), mapOptions);
	
	
	// Create the DIV to hold the control and call the CenterControl()
    // constructor passing in this DIV.
    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map);

    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(centerControlDiv);
	

	var markerPersonOptions = {
		position: new google.maps.LatLng(4.7100182257343794, -74.0325445070236),
		icon: new google.maps.MarkerImage('/themes/custom/seed_ubosque/map-svg/marker-person.svg', null, null, null, new google.maps.Size(23,25)),
		animation: google.maps.Animation.DROP
	};

	var markerPerson = new google.maps.Marker(markerPersonOptions);
	markerPerson.setMap(map);
	
	var markerPersonSmallOptions = {
		position: new google.maps.LatLng(4.711279952010147, -74.03154672527006),
		icon: new google.maps.MarkerImage('/themes/custom/seed_ubosque/map-svg/marker-person-small.svg', null, null, null, new google.maps.Size(16,18)),
		animation: google.maps.Animation.DROP
	};

	var markerPersonSmall1 = new google.maps.Marker(markerPersonSmallOptions),
		markerPersonSmall2 = new google.maps.Marker(markerPersonSmallOptions);
	markerPersonSmall1.setMap(map);
	markerPersonSmall2.setPosition(new google.maps.LatLng(4.710344350118295, -74.03028608703306));
	markerPersonSmall2.setMap(map);
	
	var markerTruckOptions = {
		position: new google.maps.LatLng(4.709349937584667, -74.03170229339293),
		icon: new google.maps.MarkerImage('/themes/custom/seed_ubosque/map-svg/marker-truck.svg', null, null, null, new google.maps.Size(23,25)),
		animation: google.maps.Animation.DROP
	};
	
	var markerTruck = new google.maps.Marker(markerTruckOptions);
	markerTruck.setMap(map);
	
	/*
	boundsBogota = {
		north: 4.711803888519607,
		south: 4.708596107781068,
		east: -74.02845145606688,
		west: -74.03317214393309
	}
	
	boundsChia = {
		north: 4.846346240929798,
		south: 4.844274957483376,
		east: -74.03072012803347,
		west: -74.03308047196657
	}*/
	
	
	// Chía
	var overlaysChia = new Array();
	for (var i=1; i < 10; i++) {
		overlaysChia[i] = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/chia-3-2-' + i + '.svg', boundsChia);
	}
	
	var fondoChia = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/chia-3-2-1.svg', boundsChia);
	fondoChia.setMap(map);
	
	// End: Chía
	
	var ov1_4_1 = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/1-4-1.svg', boundsBogota),
		ov1_4_2 = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/1-4-2.svg', boundsBogota);
	
	var overlays = new Array();
	for (var i=1; i < 43; i++) {
		overlays[i] = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/3-1-' + i + '.svg', boundsBogota);
	}
	
	var fondoOverlay = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/3-1-1.svg', boundsBogota),
		viasOverlay = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/vias.svg', boundsBogota);
	fondoOverlay.setMap(map);
	
	// puntos de encuentro
	var fondo2Overlay = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/3-1-1.svg', boundsBogota),
		puntosOverlay = new google.maps.GroundOverlay('/themes/custom/seed_ubosque/map-svg/puntos.svg', boundsBogota);
	fondo2Overlay.setMap(map);
	


	function preloadAllOverlays() {
		for (var key in overlays) {
			overlays[key].setMap(map);
		}
	}

	function hideAllOverlays() {
		viasOverlay.setMap(null),
		puntosOverlay.setMap(null);
		
		if (currentMap == cities[0]) {
			for (var key in overlays) {
				overlays[key].setMap(null);
			}
		}
		
		
		if (currentMap == cities[1]) {
			for (var key in overlaysChia) {
				overlaysChia[key].setMap(null);
			}
		}
		
		
	}

	jQuery('#check-facil-acceso').on('click', function() { // check-puntos-encuentro
		hideAllOverlays();
		viasOverlay.setMap(map);
	});
	
	jQuery('#check-puntos-encuentro').on('click', function() { // check-puntos-encuentro  <li class="list-item" id="check-puntos-encuentro"><a>Puntos de encuentro</a></li>
		hideAllOverlays();
		puntosOverlay.setMap(map);
	});

//preloadAllOverlays();
//hideAllOverlays();
	
	jQuery('[data-map]').on('click', function() {
		if (currentMap === cities[0]) {
			var mapNumber = jQuery(this).data('map');
			hideAllOverlays();
			overlays[mapNumber].setMap(map);
			
			storeLastOverlayBogota = overlays[mapNumber];
		}
		
	});
	
	jQuery('[data-chia]').on('click', function() {
		if (currentMap === cities[1]) {
			var mapNumber = jQuery(this).data('chia');
			hideAllOverlays();
			overlaysChia[mapNumber].setMap(map);
			
			storeLastOverlayChia = overlaysChia[mapNumber];
		}
	});



	jQuery('.fake-select').each(function() {
		var current = jQuery(this);
	});

	jQuery(document).on('click', '.map-navigation li', function() {
		var context = jQuery(this);
		context.addClass('active').siblings().removeClass('active');
		jQuery('.contenedor-mapa-menu').removeClass('filtros-visible'); // tablet

		// hideAll();
		if (context.attr('id') == 'check-inst-usaquen') {
			currentMap = cities[0];
			mapBogota();
			storeLastOverlayChia&&storeLastOverlayChia.setMap(null);
			jQuery('body').addClass('mapa-bogota');
			jQuery('body').removeClass('mapa-chia');
			jQuery('.filtros .map-navigation .active').removeClass('active');

			resetSelectMenu();
		}
	
		if (context.attr('id') == 'check-inst-chia') {
			currentMap = cities[1];
			mapChia();
			storeLastOverlayBogota&&storeLastOverlayBogota.setMap(null);
			jQuery('body').addClass('mapa-chia');
			jQuery('body').removeClass('mapa-bogota');
			jQuery('.filtros .map-navigation .active').removeClass('active');

			resetSelectMenu();
		} 
	});
	
	function resetSelectMenu() {
		jQuery('.f div').text('');
		jQuery('.fake-select ul li.active').removeClass('active');
		jQuery('.ul-wrapper').slideUp();
		jQuery('.fake-selected').removeClass('open');
	}

	/*
	var infoWindowOptions = {
		content: "<h1>Lol</h1><p>He he he</p>"
	};

	var infoWindow = new google.maps.InfoWindow(infoWindowOptions);*/

	google.maps.event.addListener(markerPerson, 'click', function(e) {
		infoWindow.open(map, markerPerson);
	});

	// La vista
	jQuery('.field--name-field-infraestructura-bloque').each(function() {
		var context = jQuery(this);
		context.parents('.views-row').attr('data-block', context.text());
	});
	
	var blocks = {};
	jQuery('[data-block]').each(function() {
		var context = jQuery(this).data('block');
		blocks[context] = true;
	});
	
	for (block in blocks) {
		jQuery('[data-block="' + block + '"]').wrapAll('<div id="infraestructura-block-' + block + '"><div class="js-infraestructura-block"></div></div>');
	}
	
	jQuery('.field--name-field-link a').attr('target', '_blank');
	
	jQuery('.btn-limpiar').click(function() {
		jQuery('.contenedor-mapa-menu').addClass('filtros-visible');
	});

	jQuery('.filtro-close').click(function() {
		jQuery('.contenedor-mapa-menu').removeClass('filtros-visible');
	});
	
	quickFakeSelect();
	
    function quickFakeSelect() {

        jQuery(document).on('click', '.fake-select', function() {
            var
                context = jQuery(this),
                others = jQuery('.fake-select').not(context);

            others.find('.ul-wrapper').slideUp();
            others.find('.fake-selected').removeClass('open');
        });

        jQuery(document).on('click', '.fake-select li', function() {
            var context = jQuery(this);
            context.parent().parent().parent().prev().find('.f div').text(context.text());
            context.addClass('active').siblings().removeClass('active');
        });

        jQuery(document).on('click', '.fake-selected', function() {
            var context = jQuery(this)
				contextContent = context.next().find('li:visible');
				
	            context.toggleClass('open');
	            context.next().slideToggle();
        });
    }
};


