

var initMarkers,
activeMarker,
marker,
activeInfoWindow;
function initMarkers() {
	
	//var ov_11;
	
	var markerOptions = {
		position: coordsChia, //new google.maps.LatLng(4.711782503363703, -74.0309244527786),
		icon: new google.maps.MarkerImage('/themes/custom/seed_ubosque/map-svg/marker-evil.svg', null, null, null, new google.maps.Size(18,18)),
		animation: google.maps.Animation.DROP,
		draggable: true
	};
	
	function markerHelper(lat, lng, htmlId) {
		var self = this,
		htmlId = '#infraestructura-block-' + htmlId,
		infoWindow = null;

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			icon: new google.maps.MarkerImage('/themes/custom/seed_ubosque/map-svg/marker.svg', null, null, null, new google.maps.Size(18,22)),
			animation: google.maps.Animation.DROP
		});

		//marker.addListener('click', toggleBounce);
		

		function toggleBounce() {
			if (marker.getAnimation() !== null) {
				marker.setAnimation(null);
				
			} else {
				if (jQuery(htmlId).length) {
					marker.setAnimation(google.maps.Animation.BOUNCE);
				}	
			}
		};

		self.show = function() {
			marker.setMap(map);
		    marker.setAnimation(null);
		    marker.setAnimation(google.maps.Animation.DROP);
		};

		self.hide = function() {
			//infowindow.close();
			activeInfoWindow&&activeInfoWindow.close();
			marker.setMap(null);
		};
		
		

		/*
		self.infoWindow = new google.maps.InfoWindow({
			content: jQuery(htmlId).html()
		});
		
		self.infoWindow.addListener('closeclick', function() {
			marker.setAnimation(null);
		});*/

		marker.addListener('click', function() {
			activeInfoWindow&&activeInfoWindow.close();
			activeMarker&&activeMarker.setAnimation(null);
			marker.setAnimation(google.maps.Animation.BOUNCE);
			
			infoWindow = new google.maps.InfoWindow({
				content: jQuery(htmlId).html()
			});
			
			infoWindow.addListener('closeclick', function() {
				marker.setAnimation(null);
			});

			if (jQuery(htmlId).length) {
				infoWindow.open(map, marker);
			}

			activeInfoWindow = infoWindow;
			activeMarker = marker;
		});

		
	}

	function showAll(arg) {
		var index = 0;
		// alert(arg.length);
		
		var time = setInterval(function() {
			// console.log(index);
			if (typeof arg[index] !== 'undefined') {
				arg[index].show();
			}
			
			
			//if (!arg[++index]) index = 0;
			++index;
			if (index == arg.length) clearInterval(time);
		}, 100);
		
		/*
		for (var key in arg ) {
			arg[key].show();
		}*/
	}
	var allMarkers = [];
	
	function pushApply(arr2) {
		allMarkers.push.apply(allMarkers, arr2);
	}

	
	
	
	var markerEvil = new google.maps.Marker(markerOptions);
	// markerEvil.setMap(map);
	
	
	
	
	
	/*
	// #
	var ov_ = new markerHelper();
	var overlay_ = [ov_];
	clickBtn(, overlay_);
	*/
	
	
	// # Institutos
	var ov_2_a = new markerHelper(4.709996840523554, -74.02999640845945, 'institutos1'),
		ov_2_b = new markerHelper(4.710114459175056, -74.02949751758268, 'institutos2');
		ov_2_c = new markerHelper(4.709692, -74.031956, 'institutos3'); // nuevo elemento bloque O 4.709692, -74.031956
	var overlay_2 = [ov_2_a, ov_2_b, ov_2_c];
	clickBtn(2, overlay_2);
	pushApply(overlay_2);
	
	
	
	// # Infraestructura Clínicas
	var ov_3_a = new markerHelper(4.709670715976667, -74.03151990317991, 'infraestructuraclinicas1'),
		ov_3_b = new markerHelper(4.710055649851786, -74.03101564788511, 'infraestructuraclinicas2');
	var overlay_3 = [ov_3_a, ov_3_b];
	clickBtn(3, overlay_3);
	pushApply(overlay_3);
	
	
	// # Escenarios Deportivos
	var ov_4_a = new markerHelper(4.710948481769617, -74.0315896406143,'escenariosdeportivos1'),
		ov_4_b = new markerHelper(4.711197, -74.031515,'escenariosdeportivos2'), // este es el que cambiar
		ov_4_c = new markerHelper(4.710536816896051, -74.03155208968809,'escenariosdeportivos3'),
		ov_4_d = new markerHelper(4.710258809311084, -74.03160573386839,'escenariosdeportivos4'),
		ov_4_e = new markerHelper(4.710935, -74.031456,'escenariosdeportivos5'); // este es el nuevo
	var overlay_4 = [ov_4_a, ov_4_b, ov_4_c, ov_4_d, ov_4_e];
	clickBtn(4, overlay_4);
	pushApply(overlay_4);
	
	// # Museos
	var ov_5_a = new markerHelper(4.710109112873151, -74.03126777553251,'museos1'),
		ov_5_b = new markerHelper(4.710173268493352, -74.03111757182768,'museos2'),
		ov_5_c = new markerHelper(4.709970109009067, -74.03097273254087,'museos3'),
		ov_5_d = new markerHelper(4.711244, -74.031767,'museos4'); // nuevo marcador
	var overlay_5 = [ov_5_a, ov_5_b, ov_5_c, ov_5_d];
	clickBtn(5, overlay_5);
	pushApply(overlay_5);
	
	
	// # Puntos de Comida
	var ov_6_a = new markerHelper(4.7100021868263235, -74.03237284564665, 'comida1'),
		ov_6_b = new markerHelper(4.7100021868263235, -74.03221727752378, 'comida2'),
		ov_6_c = new markerHelper(4.711242527958642, -74.03192759895018, 'comida3'),
		ov_6_d = new markerHelper(4.709574482474606, -74.03208853149107, 'comida4'),
		ov_6_e = new markerHelper(4.711365492691726, -74.03190077686003, 'comida5'),
		ov_6_f = new markerHelper(4.709462210038688, -74.03179348849943, 'comida6'),
		ov_6_g = new markerHelper(4.709900607066605, -74.03155745410612, 'comida7'),
		ov_6_h = new markerHelper(4.71050473910345, -74.03143407249144, 'comida8'),
		ov_6_i = new markerHelper(4.710697205836804, -74.03136969947508, 'comida9'),
		ov_6_j = new markerHelper(4.710494046505594, -74.03062940978697, 'comida10');
	var overlay_6 = [ov_6_a, ov_6_b, ov_6_c, ov_6_d, ov_6_e, ov_6_f, ov_6_g, ov_6_h, ov_6_i, ov_6_j];
	clickBtn(6, overlay_6);
	pushApply(overlay_6);
	
	// # Zonas de Recreación y Descanso
	var ov_7_a = new markerHelper(4.709964762706043, -74.0322387351959, 'zonas1'),
		ov_7_b = new markerHelper(4.711402916736603, -74.03199733638456, 'zonas2'),
		ov_7_c = new markerHelper(4.709595867698447, -74.03177203082731, 'zonas3'),
		ov_7_d = new markerHelper(4.710488700206602, -74.03112293624571, 'zonas4'),
		ov_7_e = new markerHelper(4.7104459298131935, -74.03079034232786, 'zonas5'),
		ov_7_f = new markerHelper(4.710098420269201, -74.03037191772154, 'zonas6'),
		ov_7_g = new markerHelper(4.710173268493352, -74.0297174587219, 'zonas7'),
		ov_7_h = new markerHelper(4.710581, -74.030544, 'zonas8');
	var overlay_7 = [ov_7_a, ov_7_b, ov_7_c, ov_7_d, ov_7_e, ov_7_f, ov_7_g, ov_7_h];
	clickBtn(7, overlay_7);
	pushApply(overlay_7);
	
	
	// # Bloques
	//var ov_8 = new markerHelper();
	var overlay_8 = [];
	clickBtn(8, overlay_8);
	
	
	
	
	// # Auditorios
	var ov_9_a = new markerHelper(4.709943377493563, -74.0320938959091, 'Auditorios1'),
		ov_9_b = new markerHelper(4.709649330755141, -74.03192759895018, 'Auditorios2'),
		ov_9_c = new markerHelper(4.709954070099892, -74.03037191772154, 'Auditorios3');
	var overlay_9 = [ov_9_a, ov_9_b, ov_9_c];
	clickBtn(9, overlay_9);	
	pushApply(overlay_9);
	
	
	// # Casas
	// var ov_ = new markerHelper();
	//var overlay_10 = [];
	//clickBtn(10, overlay_10);
	var ov_10_a = new markerHelper(4.709071929525238, -74.03192759895018, 'departamentobioetica1'), // bioetica
		ov_10_n = new markerHelper(4.708904, -74.031920, 'departamentobioetica1'), // bioetica
		ov_10_b = new markerHelper(4.708994, -74.031920, 'casamovilidad1'), // casa movilidad
		ov_10_c = new markerHelper(4.709080, -74.031489, 'casamusica1'), // casa musica
		ov_10_d = new markerHelper(4.709980, -74.029995, 'saludambiente1'), // instituto salud y ambiente
		ov_10_e = new markerHelper(4.710228, -74.030001, 'seguridadsaludtrabajo1'), // seguridad y salud en el trabajo
		ov_10_f = new markerHelper(4.710210, -74.029934, 'almacen2'), // almacen
		ov_10_g = new markerHelper(4.710200, -74.029868, 'consultoriojuridico1'), // consultorio juridico
		ov_10_h = new markerHelper(4.710182, -74.029794, 'casaimagen1'), // casa imagen
		ov_10_i = new markerHelper(4.709932684887068, -74.02972818755796, 'centrolenguas1'), // centro de lenguas
		ov_10_j = new markerHelper(4.710161, -74.029727, 'comedoresadministrativos1'), // comedores administrativos
		ov_10_k = new markerHelper(4.710150, -74.029643, 'humidades1'), // humanidades
		ov_10_l = new markerHelper(4.710114459175056, -74.02949751758268, 'institutos2'), // Instituto de neurociencias
		ov_10_m = new markerHelper(4.709590521392534, -74.02972818755796, 'centrodiseño1'); // centro de diseño
	var overlay_10 = [ov_10_a, ov_10_b, ov_10_c, ov_10_d, ov_10_e, ov_10_f, ov_10_g, ov_10_h, ov_10_i, ov_10_j, ov_10_k, ov_10_l, ov_10_m, ov_10_n];
	clickBtn(10, overlay_10);	
	pushApply(overlay_10);

	
	// #11 Infraestructura Laboratorios
	var ov_11_a = new markerHelper(4.7099754553120405, -74.03072060489347, 'infraestructuralaboratorios1'),
	ov_11_b = new markerHelper(4.709980801614976, -74.0310853853195, 'infraestructuralaboratorios2'),
	ov_11_b1 = new markerHelper(4.710119805476923, -74.03129459762266, 'infraestructuralaboratorios3'),
	ov_11_b2 = new markerHelper(4.709788334683346, -74.03131069087675, 'infraestructuralaboratorios4'),
	ov_11_c = new markerHelper(4.710820170666335, -74.03052212142637, 'infraestructuralaboratorios5'),
	ov_11_d = new markerHelper(4.7096546770605965, -74.03197051429441, 'infraestructuralaboratorios6'),
	ov_11_e = new markerHelper(4.710718591026117, -74.03111757182768, 'infraestructuralaboratorios7'),
	ov_11_f = new markerHelper(4.711365492691726, -74.03219581985167, 'infraestructuralaboratorios8'),
	ov_11_i = new markerHelper(4.710258809311084, -74.03104783439329, 'infraestructuralaboratorios9'),
	ov_11_j = new markerHelper(4.711239, -74.031767, 'infraestructuralaboratorios10'); //,
	//ov_11_k = new markerHelper(4.710258809311084, -74.03105319881132);
	
	var overlay_11 = [ov_11_a, ov_11_b, ov_11_b1, ov_11_b2, ov_11_c, ov_11_d, ov_11_e, ov_11_f, ov_11_i, ov_11_j];
	clickBtn(11, overlay_11);
	pushApply(overlay_11);
	
	/*
	// # 12 Rutas de acceso 
	var ov_ = new markerHelper();
	var overlay_ = [ov_];
	clickBtn(, overlay_);
	
	/*
	// #
	var ov_ = new markerHelper();
	var overlay_ = [ov_];
	clickBtn(, overlay_);
	
	*/
	
	// #13 Facultad de Ciencias
	var ov_13 = new markerHelper(4.70956378986245, -74.03232456588438, 'facultadciencias1');
	var overlay_13 = [ov_13];
	clickBtn(13, overlay_13);
	pushApply(overlay_13);


	// #14 Facultad de Ingeniería
	var ov_14 = new markerHelper(4.709611906615878, -74.03232456588438, 'facultadingenieria1');
	var overlay_14 = [ov_14];
	clickBtn(14, overlay_14);
	pushApply(overlay_14);
	
	// #15 Facultad de Ciencias Económicas
	var ov_15 = new markerHelper(4.709569136168554, -74.03232456588438, 'facultadcienciaseconomicas1');
	var overlay_15 = [ov_15];
	clickBtn(15, overlay_15);
	pushApply(overlay_15);
	
	
	// #16 Facultad de Ciencias Políticas
	var ov_16 = new markerHelper(4.7099754553120405, -74.03213681125334, 'facultadcienciaspoliticas1');
	var overlay_16 = [ov_16];
	clickBtn(16, overlay_16);
	pushApply(overlay_16);

	// #17 Facultad de Creación y Comunicación
	var ov_17_a = new markerHelper(4.711360146399439, -74.0322387351959, 'facultadcreacioncomunicacion1'),
		ov_17_b = new markerHelper(4.709510, -74.032334, 'facultadcreacioncomunicacion1'); //4.709510, -74.032334
	overlay_17 = [ov_17_a, ov_17_b ];
	clickBtn(17, overlay_17);
	pushApply(overlay_17);

	// #18 Facultad de Educación
	var ov_18 = new markerHelper(4.709488941572704, -74.03232993030241, 'facultadeducacion1');
	overlay_18 = [ov_18];
	clickBtn(18, overlay_18);
	pushApply(overlay_18);
	
	
	// #19 Facultad de Enfermería
	var ov_19 = new markerHelper(4.709665369671352, -74.03232993030241, 'facultadenfermeria1');
	var overlay_19 = [ov_19];
	clickBtn(19, overlay_19);
	pushApply(overlay_19);
	
	
	// #20 Facultad de Medicina
	var ov_20 = new markerHelper(4.709627945532954, -74.03232456588438, 'facultadmedicina1');
	var overlay_20 = [ov_20];
	clickBtn(20, overlay_20);
	pushApply(overlay_20);
	
	
	// #21 Facultad de Odontología
	var ov_21 = new markerHelper(4.709697447502667, -74.03232456588438, 'facultadodontologia1');
	var overlay_21 = [ov_21];
	clickBtn(21, overlay_21);
	pushApply(overlay_21);
	
	
	// #22 Facultad de Psicología
	var ov_22 = new markerHelper(4.70956378986245, -74.03231920146635, 'facultadpsicologia1');
	var overlay_22 = [ov_22];
	clickBtn(22, overlay_22);
	pushApply(overlay_22);

	// #23 Departamento de Bioética
	var ov_23 = new markerHelper(4.709071929525238, -74.03192759895018, 'departamentobioetica1');
	var overlay_23 = [ov_23];
	clickBtn(23, overlay_23);
	pushApply(overlay_23);
		
	// #23 > 40 Departamento de Humanidad (dos diseños tienen número 23)
	var ov_41 = new markerHelper(4.710135844382294, -74.02964235686949, 'departamentohumanidad1');
	var overlay_41 = [ov_41];
	clickBtn(41, overlay_41);
	pushApply(overlay_41);
	
	// #24 Educación Continuada
	var ov_24 = new markerHelper(4.709991494220733, -74.03208316707304, 'educacioncontinuada1');
	var overlay_24 = [ov_24];
	clickBtn(24, overlay_24);
	pushApply(overlay_24);
	
	// #25 Centro de Lenguas
	var ov_25 = new markerHelper(4.709932684887068, -74.02972818755796, 'centrolenguas1');
	var overlay_25 = [ov_25];
	clickBtn(25, overlay_25);
	pushApply(overlay_25);	
	
	// #26 Vicerrectoría de Investigación
	var ov_26 = new markerHelper(4.709782988378934, -74.03189004802397, 'vicerrectoriainvestigacion1');
	var overlay_26 = [ov_26];
	clickBtn(26, overlay_26);
	pushApply(overlay_26);	
	
	// #27 Unidad de apoyo
	var ov_27 = new markerHelper(4.710269501912566, -74.03126777553251, 'unidadapoyo1');
	var overlay_27 = [ov_27];
	clickBtn(27, overlay_27);
	pushApply(overlay_27);	
	
	// #28 Éxito Estudiantil
	var ov_28_a = new markerHelper(4.709466, -74.032335, 'exitoestudiantil1'),
		ov_28_b = new markerHelper(4.711098, -74.031076, 'exitoestudiantil2');
	//var overlay_28 = [ov_28];
	overlay_28 = [ov_28_a, ov_28_b ];
	clickBtn(28, overlay_28);
	pushApply(overlay_28);	
	
	
	// #29 Bienestar Universitario
	var ov_29 = new markerHelper(4.710574240985531, -74.03052212142637, 'bienestaruniversitario1');
	var overlay_29 = [ov_29];
	clickBtn(29, overlay_29);
	pushApply(overlay_29);	
	
	// #30 Centro de Atención al Usuario
	var ov_30 = new markerHelper(4.709980801614976, -74.03214217567137, 'centroatencionusuario1');
	var overlay_30 = [ov_30];
	clickBtn(30, overlay_30);
	pushApply(overlay_30);	
	
	// #31 Finanzas Estudiantiles
	var ov_31 = new markerHelper(4.709638638144131, -74.03235138797453, 'finanzasestudiantiles1');
	var overlay_31 = [ov_31];
	clickBtn(31, overlay_31);
	pushApply(overlay_31);
	
	// #32 Centro de Diseño
	var ov_32 = new markerHelper(4.709590521392534, -74.02972818755796, 'centrodiseño1');
	var overlay_32 = [ov_32];
	clickBtn(32, overlay_32);
	pushApply(overlay_32);	
	
	// #33 Talento Humano
	var ov_33 = new markerHelper(4.709574482474606, -74.0323460235565, 'talentohumano1');
	var overlay_33 = [ov_33];
	clickBtn(33, overlay_33);
	pushApply(overlay_33);	
	
	// #34 Vicerrectoría Administrativa
	var ov_34 = new markerHelper(4.709574482474606, -74.0323460235565, 'vicerrectoriaadministrativa1');
	var overlay_34 = [ov_34];
	clickBtn(34, overlay_34);
	pushApply(overlay_34);	
	
	// #35 Vicerrectoría Académica
	var ov_35 = new markerHelper(4.7096760622819565, -74.03235675239256, 'vicerrectoriaacademica1');
	var overlay_35 = [ov_35];
	clickBtn(35, overlay_35);
	pushApply(overlay_35);	
	
	// #36 Oficina de Desarrollo
	var ov_36 = new markerHelper(4.709643984449662, -74.03232456588438, 'oficinadesarrollo1');
	var overlay_36 = [ov_36];
	clickBtn(36, overlay_36);
	pushApply(overlay_36);	

	// #37 Almacén
	var ov_37 = new markerHelper(4.709782988378934, -74.03188468360594, 'almacen1');
	var overlay_37 = [ov_37];
	clickBtn(37, overlay_37);
	pushApply(overlay_37);	
	

	// #38 Servicios Generales
	var ov_38 = new markerHelper(4.709782988378934, -74.03196514987638, 'serviciosgenerales1');
	var overlay_38 = [ov_38];
	clickBtn(38, overlay_38);
	pushApply(overlay_38);	
	
	
	// #39 Desarrollo Físico
	var ov_39 = new markerHelper(4.7108789799249395, -74.03106392764738, 'desarrollofisico1');
	var overlay_39 = [ov_39];
	clickBtn(39, overlay_39);
	pushApply(overlay_39);
	
	// #40
	var ov_40 = new markerHelper(4.710173268493352, -74.02986229800871);
	var overlay_40 = [ov_40];
	clickBtn(40, overlay_40);
	pushApply(overlay_40);
	
	// #42 Rectoria 
	var ov_42 = new markerHelper(4.709977, -74.032149, 'rectoria1'); // 4.709943, -74.032262
	var overlay_42 = [ov_42];
	clickBtn(42, overlay_42);
	pushApply(overlay_42);
	
	// Chía 1
	// var cov_1 = new markerHelper(4.8457168386958305, -74.03172327420509);
	/*var coverlay_1 = [];
	clickBtn(1, coverlay_1);
	pushApply(coverlay_1);*/
	
	
	// Chía 2: Auditorios
	var cov_2 = new markerHelper(4.8457168386958305, -74.03172327420509, 'chiaauditorios1');
	var coverlay_2 = [cov_2];
	clickBtn(2, coverlay_2);
	pushApply(coverlay_2);
	
	// Chía 3: Puntos de Comida
	var cov_3a = new markerHelper(4.8458531424140485, -74.0316696300248, 'chiacomida1'),
		cov_3b = new markerHelper(4.845695457717915, -74.03164012572563, 'chiacomida2');
	var coverlay_3 = [cov_3a, cov_3b];
	clickBtn(3, coverlay_3);
	pushApply(coverlay_3);
	
	// Chía 5: Enfermería 
	var cov_5 = new markerHelper(4.845561826590701, -74.03150601527489, 'chiaenfermeria1');
	var coverlay_5 = [cov_5];
	clickBtn(5, coverlay_5);
	pushApply(coverlay_5);
	
	// Chía 6: Servicios Generales 
	var cov_6 = new markerHelper(4.845561826590701, -74.03150601527489, 'chiaserviciosgenerales1');
	var coverlay_6 = [cov_6];
	clickBtn(6, coverlay_6);
	pushApply(coverlay_6);
	
	// Chía 7: Escenarios deportivos
	var cov_7a = new markerHelper(4.84527852851362, -74.03212560555733, 'chiaescenariosdeportivos1'),
		cov_7b = new markerHelper(4.845599243308979, -74.03171522757805, 'chiaescenariosdeportivos2');
		var coverlay_7 = [cov_7a, cov_7b];
		clickBtn(7, coverlay_7);
		pushApply(coverlay_7);
		
	// Chía 8: Zonas de descanso y recreación
	var cov_8a = new markerHelper(4.8457168386958305, -74.03208805463112, 'chiazonasdescanso1'),
		cov_8b = new markerHelper(4.845895904359204, -74.03199954173363, 'chiazonasdescanso2'),
		cov_8c = new markerHelper(4.845815725709828, -74.03191907546318, 'chiazonasdescanso3'),
		cov_8d = new markerHelper(4.845024629192693, -74.03190030000007, 'chiazonasdescanso4'),
		cov_8e = new markerHelper(4.8458852138731725, -74.03175546071327, 'chiazonasdescanso5'),
		cov_8f = new markerHelper(4.845973410377896, -74.03162939688957, 'chiazonasdescanso6');

		var coverlay_8 = [cov_8a, cov_8b, cov_8c, cov_8d, cov_8e, cov_8f];
		clickBtn(8, coverlay_8);
		pushApply(coverlay_8);
	
	// Chía 9:  Laboratorios
	var cov_9 = new markerHelper(4.845235766529408, -74.03178228280342, 'chialaboratorios1');
		var coverlay_9 = [cov_9];
		clickBtn(9, coverlay_9);
		pushApply(coverlay_9);
	
	document.getElementById('check-facil-acceso').onclick = hideAll;
	
	document.getElementById('check-puntos-encuentro').onclick = hideAll;
	
	
	function hideAll() {
		for (var i = 0; i < allMarkers.length; i++) {
			var markers = allMarkers[i];
			markers.hide();
			for (var j = 0; j < markers.length; j++) {
				markers[j].hide(); //.setMap(null);
			}
		}
	}
	
	
	function clickBtn(btn, ov) {
		jQuery(document).on('click', '[data-map="' + btn + '"]', function() {
			if (currentMap == cities[0]) {
				hideAll();	
				showAll(ov);
			}
		});
		
		jQuery(document).on('click', '[data-chia="' + btn + '"]', function() {
			if (currentMap == cities[1]) {
				hideAll();	
				showAll(ov);
			}
		});
	}
	
	
	
	/*
	function clickBtn(btn, ov) {
		if (jQuery('.mapa-bogota').length == 1) {
			jQuery(document).on('click', '[data-map="' + btn + '"]', function() {
				hideAll();	
				showAll(ov);
				alert(currentMap);
			});
		}
		
		if (jQuery('.mapa-chia').length == 1) {
			alert('z2');
			jQuery(document).on('click', '[data-chia="' + btn + '"]', function() {
				hideAll();	
				showAll(ov);
				alert('zzz');
			});
		}
	}
	*/
	
	
	// Cuando cambiamos de mapa removemos todos los markers
	jQuery(document).on('click', '.map-navigation li', function() {
		var context = jQuery(this);
		if (context.attr('id') == 'check-inst-usaquen') {
			hideAll();
		}
	
		if (context.attr('id') == 'check-inst-chia') {
			hideAll();	
		} 
	});

	/*
	jQuery('<div style="overflow: hidden">Lat: <input type="text" id="lat" onfocus="this.select()"> | Lng: <input id="lng" />').insertBefore('#map');

	google.maps.event.addListener(markerEvil, 'drag', function(e) {
		document.getElementById('lat').value = e.latLng.lat() + ', ' + e.latLng.lng();
		// document.getElementById('lng').value = e.latLng.lng();
	});*/
};

window.onload = function() {
	initMap();
	initMarkers();
	jQuery('body').addClass('mapa-bogota');
};
